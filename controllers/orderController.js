// [SECTION] Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require('bcrypt');
const auth = require("../auth");

// [SECTION] Route for creating order
/*
	1. Find the document in the database using the user's ID
	2. Add the product ID to Orders
	3. Update the document in the MongoDB Atlas/Compass database
*/

module.exports.createOrder = (req, res) => {
	let newOrder = new Order({
		userId : req.user.id,
		products : req.body.products,
		totalAmount : req.body.totalAmount,
		purchasedOn : req.body.purchasedOn
	})
	// saves the created object to our database
	return newOrder.save().then((order, error) => {
		// Order failed
		if(error){
			return res.send(false);
		// Order successful
		}else{
			// return res.send(order);
			return res.json({ message: 'Order received!' });
		}
	}).catch(err => res.send(err))
}

// Stretch Goals
// [SECTION] Retrieve authenticated user's orders
module.exports.getUserOrder = (req, res) => {
	return Order.find({userId : req.user.id}).then(result => {
	    return res.send(result)
	})
}

// [SECTION] Retrieve all orders (admin only)
module.exports.getAllOrders = (req, res) => {
	return Order.find({}).then(result => {
		return res.send(result);
	})
}