// [SECTION] Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require('bcrypt');
const auth = require("../auth");


module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password : reqBody.password,
		// 10 is the value provided as the number of "salt" rounds that the bycrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false
		// User registration successful
		}else{
			return true
		}
	}).catch(err => err)
}

// [SECTION] User authentication
/*
	Steps:
	1. Check the database if user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.loginUser = (req, res) => {
	return User.findOne({email : req.body.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		// User exists
		}else{
			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// the "compareSync" method is used to compare non encrypted password from the login from to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect){
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				return res.send({access : auth.createAccessToken(result)})
			//Passwords do not match 
			}else {
				return res.send(false);
			}
		}
	}).catch(err => res.send(err))
}

	
// Retrieve user details
/*
    Steps:
    1. Find the document in the database using the user's ID
    2. Reassign the password of the returned document to an empty string
    3. Return the result back to the frontend
*/
module.exports.getProfile = (req, res) => {


    return User.findById(req.user.id)
    .then(result => {

        // Changes the value of the user's password to an empty string when returned to the frontend
        // Not doing so will expose the user's password which will also not be needed in other parts of our application
        // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
        result.password = "";

        // Returns the user information with the password as an empty string
        return res.send(result);

    })
    .catch(err => res.send(err))
};


// Stretch Goals
// [SECTION] Route for updating a user as an admin
module.exports.updateUserAsAdmin = async (req, res) => {

	// Find the user to be updated by their ID
	const userToUpdate = await User.findById(req.body.id);

	if (!userToUpdate) {
	return res.status(404).json({ message: 'User not found.' });
	}

	// Update the user's isAdmin field to true
	userToUpdate.isAdmin = true;
	await userToUpdate.save();

	return res.json({ message: 'User updated as admin successfully.' });
}

