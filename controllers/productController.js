// [SECTION] Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.addProduct = (req, res) => {

    let newProduct = new Product({
        name : req.body.name,
        description : req.body.description,
        price : req.body.price
    });

    // Saves the created product to our database
    return newProduct.save().then((product, error) => {

        // Product creation successful
        if (error) {
            return res.send(false);

        // Product creation failed
        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err))
};

// Retrieve all products
module.exports.getAllProducts = (req, res) => {
    // empty {} will return all products
    return Product.find({}).then(result => {
        return res.send(result)
    })
    .catch(err => res.send(err))
}

// Retrieve all active products
module.exports.getAllActive = (req, res) => {
    return Product.find({isActive : true}).then(result => {
        return res.send(result)
    })
}

// Retrieving a single product
module.exports.getProduct = (req, res) => {
    // We can retrieve the product ID by accessing the request's "params" property which contains all the parameters provided via URL
    return Product.findById(req.params.productId).then(result => {
        return res.send(result);
    })
}

// Updating a product (Admin only)
/*
    1. Create a variable updateProduct which will contain the information retrieved from the request body
    2. find and update the product using the product ID retrieved from the request params property and the variable "updateProduct" containing the information from the request body
*/
module.exports.updateProduct = (req, res) => {
    let updatedProduct = {
        name : req.body.name,
        description : req.body.description,
        price : req.body.price
    };

    return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {
        if(error){
            return res.send(false)
        }else{
            return res.send(true)
        }
    })
}

// Archiving a Product
module.exports.archiveProduct = (req, res) => {

    let updateActiveField = {
        isActive: false
    }

    return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
    .then((product, error) => {

        //product archived successfully
        if(error){
            return res.send(false)

        // failed
        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};

// module.exports.archiveProduct = (req, res) => {
//     let statusProduct = {
//         isActive: req.body.isActive
//     };
//     return Product.findByIdAndUpdate(req.params.productId, statusProduct).then((product, error) => {
//         if (error) {
//             return res.send(false)
//         }
//         else {
//             return res.send(true)
//         }
//     })

// }



// Activating a Product
module.exports.activateProduct = (req, res) => {

    let updateActiveField = {
        isActive: true
    }

    return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
        .then((product, error) => {

        //product archived successfully
        if(error){
            return res.send(false)

        // failed
        } else {
            return res.send(true)
            }
    })
    .catch(err => res.send(err))

};

// module.exports.activateProduct = (req, res) => {
//     let updatedIsActive = {
        
//         isActive : true
//     }

//     return Product.findByIdAndUpdate(req.params.productId, updatedIsActive).then((product, error)=>{
//         if(error){
//             return res.send(false)
//         }else{
//             return res.send(true)
//         }
//     })
// }

