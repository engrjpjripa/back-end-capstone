// [SECTION] Dependencies and Modules
const mongoose = require("mongoose");

// [SECTION] Schema/Blueprint
const productSchema =  new mongoose.Schema({
	name : {
		type: String,
		required: [true, "name is required"]
	},
	description: {
		type: String,
		required: [true, "description is required"]
	},
	price: {
		type: Number,
		required: [true, "price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Product", productSchema);


