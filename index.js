// [SECTION]Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

// Allows access to routes defined within our application
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

// [SECTION]Environment Setup
const port = 4021;

// [SECTION]Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
// Allows all resources to access our backend application
app.use(cors());

// [SECTION] Backend Routes
// Defines the "/users" string to be included for all user routes defined in the "user" route
app.use("/b21/users", userRoutes);
// Defines the "/products" string to be included for all product routes defined in the "product" route
app.use("/b21/products", productRoutes);
// Defines the "/orders" string to be included for all order routes defined in the "order" route
app.use("/b21/orders", orderRoutes);


// [SECTION] Database Connection
mongoose.connect("mongodb+srv://engrjpjripa:admin123@cluster0.ly9ni3p.mongodb.net/E-commerce_API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));


// [SECTION]Server gateway response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}

// export mongoose only for checking
module.exports = {app, mongoose};
