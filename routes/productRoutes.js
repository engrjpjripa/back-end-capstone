// [SECTION] Dependencies and Modules
const express = require("express");
const productController = require("../controllers/productController");
const auth = require('../auth');

const {verify, verifyAdmin} = auth;


// [SECTION] Routing Component
const router = express.Router();


//[SECTION] create a product POST for admin
router.post("/", verify, verifyAdmin, productController.addProduct)


// [SECTION] Route for retrieving all the products
router.get("/all", productController.getAllProducts);


// [SECTION] Route for retrieving all ACTIVE products for all users
router.get("/", verify, verifyAdmin, productController.getAllActive);


// [SECTION] Route for retrieving a single product
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the URL is not static and changes depending on the information provided in the URL
router.get("/:productId", productController.getProduct);


// Route for updating a product(Admin only)
// Add JWT authentication for verifying if it is an admin or not
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);


// Route for Archiving a Product
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct)


// Activating a Product
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct)

// [SECTION] Export Route System
// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;