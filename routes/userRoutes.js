// [SECTION] Dependencies and Modules
const express = require("express");
const userController = require("../controllers/userController");
const auth = require('../auth');

const {verify, verifyAdmin} = auth;


// [SECTION] Routing Component
const router = express.Router();


// [SECTION] Routes
//Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// [SECTION] Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})


// [SECTION] Route for user authentication
router.post("/login", userController.loginUser);


//[SECTION] Route for retrieving user details
// the "verify" acts as a middleware to ensure that the user is logged in before they can enroll to a course
// since we have next() used in verify in auth, the next() will let us proceed to the next function which is the userController.getProfile
router.get("/details", verify, userController.getProfile);


// Stretch Goals
// [SECTION] Route for updating a user as an admin
router.put('/updateAdmin', verify, verifyAdmin, userController.updateUserAsAdmin);

	


    

// [SECTION] Export Route System
	// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;