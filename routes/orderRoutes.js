// [SECTION] Dependencies and Modules
const express = require("express");
const orderController = require("../controllers/orderController");
const auth = require('../auth');

const {verify, verifyAdmin} = auth;


// [SECTION] Routing Component
const router = express.Router();

// [SECTION] Route for creating order
router.post("/checkout", verify, orderController.createOrder);

// Stretch Goals
// [SECTION] Retrieve authenticated user's orders
router.get('/userOrder', verify, orderController.getUserOrder);

// [SECTION] Retrieve all orders (admin only)
router.get('/all', verify, verifyAdmin, orderController.getAllOrders);


    

// [SECTION] Export Route System
	// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;